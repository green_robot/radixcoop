# Simulation Coop

## Unity

#### Download
- [Personal Edition](https://store.unity.com/download?ref=personal)

#### Concepts
- [Roll-A-Ball Tutorial](https://unity3d.com/learn/tutorials/projects/roll-ball-tutorial)
- [Physics Tutorial](https://unity3d.com/learn/tutorials/topics/physics)

#### References
Unity documentation is divided into Scripting API and Manual

- [Manual explains Unity Concepts](https://docs.unity3d.com/Manual/UnityOverview.html)
- [Scripting provides API commands and examples](https://docs.unity3d.com/ScriptReference/index.html)


## Git
Git is version control: it keeps track of changes in your source code files. All modern software development uses a Version Control System (VCS) and Git is the most popular.

- [GitHub Tutorial](https://try.github.io/levels/1/challenges/1)
- [Git Overview](http://rogerdudler.github.io/git-guide/)
- [Cheat Sheet](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf)

## C# (C Sharp)
C# is a powerful general purpose programming language from Microsoft. It is what Unity uses. There are many ways to learn C# online so you can choose whichever works best for you. (You may want to make a cheat sheet for yourself.)

- [TutorialsPoint](https://www.tutorialspoint.com/csharp/)

## Markdown
Markdown is a way to format text without using a word processor. It is entirely text based. You write it like code but it's used only for documentation. It is what the README files in git repos are written in.

- [Cheat Sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## Editors
Editors are used to write code (and Markdown). They're like a powerful Notepad. There are a bunch to choose from but here's our preference in order:

- [Microsoft VS Code](https://code.visualstudio.com/)
- MonoDevelop (comes with Unity)
- [Microsoft Visual Studio](https://www.visualstudio.com/)
- [Sublime Text](https://www.sublimetext.com/)

